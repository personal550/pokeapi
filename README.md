# Poke api

## Description
This project allow search all pokemon and view their evolutions

### Pre-requirements 📋
- Java 11 or higher
- Git installed
- GitBash - download in this https://gitforwindows.org/
- 
## Setup 📦
- Open terminal or cmd
- Clone the project with the command "git clone https://gitlab.com/personal550/pokeapi.git"
- Navigate to folder called "pokeapi" with command "cd pokeapi"
- Run linux command  "./mvnw clean install" or System window Run next command  "mvnw.cmd clean install"
- Run command "java -jar target/poke-api.jar"

## Components diagram
![diagram](img/diagram.png)

## Deploy server steps
- You can visit this link if you want to read about deploy this project -> https://www.evernote.com/shard/s589/sh/b01797bd-e1c4-4271-94b1-a6f3eff1828e/107796ebb1d5ec6618f11c2ee243f873

## Documentation
- You can visit documentation for more information about the API in this link
    local: http://localhost:8080/swagger-ui.html
    remote: https://personal-app-ga.herokuapp.com/swagger-ui.html


## Usage
1.- Find all Pokemon with a specific page

    -url local : http://localhost:8080/api/v1/pokemon/page/<page>/limit/<number_of_records_by_page>
    -url heroku : https://personal-app-ga.herokuapp.com/api/v1/pokemon/page/<page>/limit/<number_of_records_by_page>
    -http method: GET

Example:
![pokemonbypage](img/pokemonbypage.png)


2.- Find all evolution of a specific Pokemon

    -url local : http://localhost:8080/api/v1/pokemon/envolve/<id_pokemon>
    -url heroku: https://personal-app-ga.herokuapp.com/api/v1/pokemon/envolve/<id_pokemon>
    -http method: GET

Example:
![pokemonevoluction](img/pokemonevolution.png)


## Build with 🛠️
- Java
- Spring boot
- Junit
- JPA