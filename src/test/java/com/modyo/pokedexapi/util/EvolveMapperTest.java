package com.modyo.pokedexapi.util;

import static org.junit.jupiter.api.Assertions.*;

import com.modyo.pokedexapi.data.dto.response.evolution.Chain;
import com.modyo.pokedexapi.data.dto.response.evolution.EvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.Specie;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class EvolveMapperTest {
  final String jsonEvolutionChain = "{ \"abilities\": [ { \"ability\": { \"name\": \"blaze\", \"url\": \"https://pokeapi.co/api/v2/ability/66/\" }, \"is_hidden\": false, \"slot\": 1 }, { \"ability\": { \"name\": \"solar-power\", \"url\": \"https://pokeapi.co/api/v2/ability/94/\" }, \"is_hidden\": true, \"slot\": 3 } ], \"height\": 6, \"held_items\": [], \"id\": 4, \"name\": \"charmander\", \"order\": 5, \"past_types\": [], \"species\": { \"name\": \"charmander\", \"url\": \"https://pokeapi.co/api/v2/pokemon-species/4/\" }, \"sprites\": { \"back_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/4.png\", \"back_female\": null, \"back_shiny\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/4.png\", \"back_shiny_female\": null, \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png\", \"front_female\": null, \"front_shiny\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/4.png\", \"front_shiny_female\": null, \"other\": { \"dream_world\": { \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg\", \"front_female\": null }, \"home\": { \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/4.png\", \"front_female\": null, \"front_shiny\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/shiny/4.png\", \"front_shiny_female\": null }, \"official-artwork\": { \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/4.png\" } } }, \"types\": [ { \"slot\": 1, \"type\": { \"name\": \"fire\", \"url\": \"https://pokeapi.co/api/v2/type/10/\" } } ], \"weight\": 85 }";

  @Test
  void should_returnAListOfSpecies_when_speciesExist() {
    ResponsePokeApiEvolveTo r = responsePokeApiEvolveTo();
    List<Specie> response = InvolveMapper.mapInvolveResponseToListEvolves(r);
    int expected = 1;
    int actual = response.size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnAListOfSpecies_when_evolveDoesNotExist() {
    ResponsePokeApiEvolveTo r = responsePokeApiEvolveTo();
    r.getChain().setEvolvesTo(new ArrayList<>());
    List<Specie> response = InvolveMapper.mapInvolveResponseToListEvolves(r);
    int expected = 1;
    int actual = response.size();
    assertEquals(expected, actual);
  }

  private ResponsePokeApiEvolveTo responsePokeApiEvolveTo() {
    ResponsePokeApiEvolveTo response = new ResponsePokeApiEvolveTo();
    Chain chain = new Chain();
    Specie species = new Specie();
    species.setName("charmander");
    species.setPhoto("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/10036.png");
    chain.setSpecies(species);
    EvolveTo evolveTo = new EvolveTo();
    chain.setEvolvesTo(new ArrayList<>(){
      {
        add(evolveTo);
      }
    });
    response.setChain(chain);
    return response;
  }

}