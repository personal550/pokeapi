package com.modyo.pokedexapi.business.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.modyo.pokedexapi.business.service.CharacteristicService;
import com.modyo.pokedexapi.business.service.EvolutionService;
import com.modyo.pokedexapi.configuration.feign.PokeApi;
import com.modyo.pokedexapi.configuration.property.ServiceInformationProperties;
import com.modyo.pokedexapi.data.dto.request.RequestAllPokemon;
import com.modyo.pokedexapi.data.dto.response.evolution.AbilityEvolution;
import com.modyo.pokedexapi.data.dto.response.evolution.Effect;
import com.modyo.pokedexapi.data.dto.response.evolution.EvolutionChain;
import com.modyo.pokedexapi.data.dto.response.evolution.Language;
import com.modyo.pokedexapi.data.dto.response.evolution.PokemonSpecies;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponseApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.Specie;
import com.modyo.pokedexapi.data.dto.response.pokemon.Ability;
import com.modyo.pokedexapi.data.dto.response.pokemon.Pokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseApiPokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseCharacteristicApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokeApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.Species;
import com.modyo.pokedexapi.data.dto.response.pokemon.Type;
import com.modyo.pokedexapi.util.InvolveMapper;
import com.modyo.pokedexapi.util.ResponsePokemonBuild;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PokemonServiceImplTest {

  @Mock
  private PokeApi pokeApi;

  @Mock
  private ServiceInformationProperties serviceInformation;

  @Mock
  private EvolutionService evolutionService;

  @Mock
  private CharacteristicService characteristicService;


  @InjectMocks
  private PokemonServiceImpl pokemonService = new PokemonServiceImpl();

  final String jsonAllPokemon = "{ \"count\": 1154, \"next\": \"https://pokeapi.co/api/v2/pokemon?offset=30&limit=20\", \"previous\": \"https://pokeapi.co/api/v2/pokemon?offset=0&limit=10\", \"results\": [ { \"name\": \"metapod\", \"url\": \"https://pokeapi.co/api/v2/pokemon/11/\" }, { \"name\": \"butterfree\", \"url\": \"https://pokeapi.co/api/v2/pokemon/12/\" }, { \"name\": \"weedle\", \"url\": \"https://pokeapi.co/api/v2/pokemon/13/\" }, { \"name\": \"kakuna\", \"url\": \"https://pokeapi.co/api/v2/pokemon/14/\" }, { \"name\": \"beedrill\", \"url\": \"https://pokeapi.co/api/v2/pokemon/15/\" }, { \"name\": \"pidgey\", \"url\": \"https://pokeapi.co/api/v2/pokemon/16/\" }, { \"name\": \"pidgeotto\", \"url\": \"https://pokeapi.co/api/v2/pokemon/17/\" }, { \"name\": \"pidgeot\", \"url\": \"https://pokeapi.co/api/v2/pokemon/18/\" }, { \"name\": \"rattata\", \"url\": \"https://pokeapi.co/api/v2/pokemon/19/\" }, { \"name\": \"raticate\", \"url\": \"https://pokeapi.co/api/v2/pokemon/20/\" }, { \"name\": \"spearow\", \"url\": \"https://pokeapi.co/api/v2/pokemon/21/\" }, { \"name\": \"fearow\", \"url\": \"https://pokeapi.co/api/v2/pokemon/22/\" }, { \"name\": \"ekans\", \"url\": \"https://pokeapi.co/api/v2/pokemon/23/\" }, { \"name\": \"arbok\", \"url\": \"https://pokeapi.co/api/v2/pokemon/24/\" }, { \"name\": \"pikachu\", \"url\": \"https://pokeapi.co/api/v2/pokemon/25/\" }, { \"name\": \"raichu\", \"url\": \"https://pokeapi.co/api/v2/pokemon/26/\" }, { \"name\": \"sandshrew\", \"url\": \"https://pokeapi.co/api/v2/pokemon/27/\" }, { \"name\": \"sandslash\", \"url\": \"https://pokeapi.co/api/v2/pokemon/28/\" }, { \"name\": \"nidoran-f\", \"url\": \"https://pokeapi.co/api/v2/pokemon/29/\" }, { \"name\": \"nidorina\", \"url\": \"https://pokeapi.co/api/v2/pokemon/30/\" } ] }";
  final String jsonEvolutionChain = "{ \"abilities\": [ { \"ability\": { \"name\": \"blaze\", \"url\": \"https://pokeapi.co/api/v2/ability/66/\" }, \"is_hidden\": false, \"slot\": 1 }, { \"ability\": { \"name\": \"solar-power\", \"url\": \"https://pokeapi.co/api/v2/ability/94/\" }, \"is_hidden\": true, \"slot\": 3 } ], \"height\": 6, \"held_items\": [], \"id\": 4, \"name\": \"charmander\", \"order\": 5, \"past_types\": [], \"species\": { \"name\": \"charmander\", \"url\": \"https://pokeapi.co/api/v2/pokemon-species/4/\" }, \"sprites\": { \"back_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/4.png\", \"back_female\": null, \"back_shiny\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/4.png\", \"back_shiny_female\": null, \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png\", \"front_female\": null, \"front_shiny\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/4.png\", \"front_shiny_female\": null, \"other\": { \"dream_world\": { \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg\", \"front_female\": null }, \"home\": { \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/4.png\", \"front_female\": null, \"front_shiny\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/shiny/4.png\", \"front_shiny_female\": null }, \"official-artwork\": { \"front_default\": \"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/4.png\" } } }, \"types\": [ { \"slot\": 1, \"type\": { \"name\": \"fire\", \"url\": \"https://pokeapi.co/api/v2/type/10/\" } } ], \"weight\": 85 }";

  @BeforeEach
  public void setup() {
    when(evolutionService.getAllEvolutionsChainOfAPokemonByUrl(any())).thenReturn(
        responsePokeApiEvolveTo());
    when(evolutionService.getUrlEvolutionOfAPokemonByUrl(any())).thenReturn(getPokemonSpecies());

    when(characteristicService.getCharacteristicsByIdPokemon(anyInt())).thenReturn(
        getResponsePokemon());
    when(characteristicService.getCharacteristicsFromListOfPokemon(any())).thenReturn(
        getListResponsePokemon());
    when(characteristicService.getAllInformationOfAPokemon(anyString())).thenReturn(
        getResponseCharacteristicApi());

    when(serviceInformation.getUrlService()).thenReturn("https://pokeapi.co");
    when(serviceInformation.getEndpointFindById()).thenReturn("api/v2/pokemon/");
    when(serviceInformation.getEndpointPokemon()).thenReturn(
        "api/v2/pokemon?offset={offset}&limit={limit}");
    when(serviceInformation.getEndpointAbility()).thenReturn("/api/v2/ability/{idPokemon}/");
    when(serviceInformation.getEndpointCharacteristic()).thenReturn(
        "/api/v2/pokemon/{pokemonName}/");
    when(serviceInformation.getEndpointEvolutions()).thenReturn(
        "/api/v2/evolution-chain/{idPokemon}/");
    when(serviceInformation.getLimit()).thenReturn(20);
    when(pokeApi.getAbility(anyInt())).thenReturn(getAbilityEvolution());


  }

  @Test
  void should_throwException_when_getAbilityServiceWasNotExecutedSuccessfully() {
    when(pokeApi.getAbility(anyInt())).thenThrow(new RuntimeException(""));
    try (MockedStatic mocked = mockStatic(InvolveMapper.class)) {
      Specie specie1 = new Specie();
      List<Specie> species = new ArrayList<>();
      species.add(specie1);
      mocked.when(() -> InvolveMapper.mapInvolveResponseToListEvolves(any())).thenReturn(species);
      try (MockedStatic mocked2 = mockStatic(ResponsePokemonBuild.class)) {
        mocked.when(() -> ResponsePokemonBuild.getPhotoFromResponse(any())).thenReturn("");
        try{
          pokemonService.getEvolutionById(1);
        }catch(ResponseStatusException e){
          String expected = "";
          String actual = e.getMessage();
          assertEquals(expected, actual);
        }
      }
    }
  }


  @Test
  void should_AListAllEvolutions_when_serviceEvolutionsResponseOk() {
    try (MockedStatic mocked = mockStatic(InvolveMapper.class)) {
      Specie specie1 = new Specie();
      List<Specie> species = new ArrayList<>();
      species.add(specie1);
      mocked.when(() -> InvolveMapper.mapInvolveResponseToListEvolves(any())).thenReturn(species);
      try (MockedStatic mocked2 = mockStatic(ResponsePokemonBuild.class)) {
        mocked.when(() -> ResponsePokemonBuild.getPhotoFromResponse(any())).thenReturn("");
        ResponseApiEvolveTo response = pokemonService.getEvolutionById(1);
        int expected = 1;
        int actual = response.getEvolves().size();
        assertEquals(expected, actual);
      }
    }
  }


  @Test
  void should_AListEmpty_when_serviceByPageWasNotExecutedSuccessfully() {
    when(pokeApi.getByOffsetAndPage(anyInt(), anyInt())).thenReturn(getResponsePokeApi());
    when(pokeApi.getAbility(anyInt())).thenReturn(getAbilityEvolution());
    ResponseApiPokemon response = pokemonService.getByOffsetAndPage(
        RequestAllPokemon.builder().page(1).limit(2).build());
    int expected = 1;
    int actual = response.getPokemon().size();
    assertEquals(expected, actual);
  }

  @Test
  void should_AListOfPokemon_when_serviceReturnManyPokemon() {
    Optional<ResponsePokeApi> responsePokeApi = getResponsePokeApi();
    responsePokeApi.get().setResults(new ArrayList<>() {
      {
        add(Pokemon.builder().name("dugtrio").url("https://pokeapi.co/api/v2/pokemon/51/").build());
        add(Pokemon.builder().name("meowth").url("https://pokeapi.co/api/v2/pokemon/52/").build());
      }
    });
    when(pokeApi.getByOffsetAndPage(anyInt(), anyInt())).thenReturn(responsePokeApi);
    when(pokeApi.getAbility(anyInt())).thenReturn(getAbilityEvolution());
    ResponseApiPokemon response = pokemonService.getByOffsetAndPage(
        RequestAllPokemon.builder().page(1).limit(20).build());
    int expected = 1;
    int actual = response.getPokemon().size();
    assertEquals(expected, actual);
  }

  private List<Specie> listOfSpecies(){
    Specie specie = new Specie();
    specie.setName("name");
    specie.setDescription("description");
    specie.setPhoto("");
    return new ArrayList<>(){
      {
        add(new Specie());
      }
    };
  }

  private ResponseCharacteristicApi getResponseCharacteristicApi() {
    final Gson gson = new GsonBuilder().create();
    final ResponseCharacteristicApi response = gson.fromJson(jsonAllPokemon,
        ResponseCharacteristicApi.class);
    return response;
  }

  private List<ResponsePokemon> getListResponsePokemon() {
    return new ArrayList<>() {
      {
        add(new ResponsePokemon());
      }
    };
  }

  private ResponsePokemon getResponsePokemon() {
    ResponsePokemon response = new ResponsePokemon();
    response.setIdPokemon(1);
    response.setName("pokemon");
    response.setPhoto("");
    Species species = new Species();
    species.setName("");
    species.setUrl("");
    response.setSpecies(species);
    Ability ability = new Ability();
    ability.setName("");
    ability.setUrl("");
    response.setAbilities(new ArrayList<>() {
      {
        add(ability);
      }
    });
    Type type = new Type();
    type.setName("");
    type.setUrl("");
    response.setTypes(new ArrayList<>() {
      {
        add(type);
      }
    });
    return response;
  }

  private PokemonSpecies getPokemonSpecies() {
    PokemonSpecies response = new PokemonSpecies();
    EvolutionChain evolutionChain = new EvolutionChain();
    evolutionChain.setUrl("https://pokeapi.co/api/v2/type/13/");
    response.setEvolutionChain(evolutionChain);
    return response;
  }

  private ResponsePokeApiEvolveTo responsePokeApiEvolveTo() {
    final Gson gson = new GsonBuilder().create();
    final ResponsePokeApiEvolveTo response = gson.fromJson(jsonEvolutionChain,
        ResponsePokeApiEvolveTo.class);
    return response;
  }

  private Optional<ResponsePokeApi> getResponsePokeApi() {
    final Gson gson = new GsonBuilder().create();
    final ResponsePokeApi response = gson.fromJson(jsonAllPokemon, ResponsePokeApi.class);
    return Optional.ofNullable(response);
  }

  private Optional<AbilityEvolution> getAbilityEvolution() {
    return Optional.ofNullable(
        AbilityEvolution
            .builder()
            .effect_entries(new ArrayList<>() {
              {
                add(Effect.builder()
                    .language(Language.builder().name("en").url(
                            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/112.png")
                        .build())
                    .short_effect("Ensures success fleeing from wild battles.")
                    .build());
              }
            })
            .build()
    );
  }
}