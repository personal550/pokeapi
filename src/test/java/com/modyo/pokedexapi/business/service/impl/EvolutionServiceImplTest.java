package com.modyo.pokedexapi.business.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.modyo.pokedexapi.data.dto.response.evolution.EvolutionChain;
import com.modyo.pokedexapi.data.dto.response.evolution.PokemonSpecies;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.web.client.RestTemplate;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class EvolutionServiceImplTest {

  @Mock
  private RestTemplate restTemplate;

  @InjectMocks
  private EvolutionServiceImpl evolutionService = new EvolutionServiceImpl();

  final String jsonEvolutionChain = "{ \"baby_trigger_item\": null, \"chain\": { \"evolution_details\": [], \"evolves_to\": [ { \"evolution_details\": [ { \"gender\": null, \"held_item\": null, \"item\": null, \"known_move\": null, \"known_move_type\": null, \"location\": null, \"min_affection\": null, \"min_beauty\": null, \"min_happiness\": null, \"min_level\": 16, \"needs_overworld_rain\": false, \"party_species\": null, \"party_type\": null, \"relative_physical_stats\": null, \"time_of_day\": \"\", \"trade_species\": null, \"trigger\": { \"name\": \"level-up\", \"url\": \"https://pokeapi.co/api/v2/evolution-trigger/1/\" }, \"turn_upside_down\": false } ], \"evolves_to\": [ { \"evolution_details\": [ { \"gender\": null, \"held_item\": null, \"item\": null, \"known_move\": null, \"known_move_type\": null, \"location\": null, \"min_affection\": null, \"min_beauty\": null, \"min_happiness\": null, \"min_level\": 36, \"needs_overworld_rain\": false, \"party_species\": null, \"party_type\": null, \"relative_physical_stats\": null, \"time_of_day\": \"\", \"trade_species\": null, \"trigger\": { \"name\": \"level-up\", \"url\": \"https://pokeapi.co/api/v2/evolution-trigger/1/\" }, \"turn_upside_down\": false } ], \"evolves_to\": [], \"is_baby\": false, \"species\": { \"name\": \"charizard\", \"url\": \"https://pokeapi.co/api/v2/pokemon-species/6/\" } } ], \"is_baby\": false, \"species\": { \"name\": \"charmeleon\", \"url\": \"https://pokeapi.co/api/v2/pokemon-species/5/\" } } ], \"is_baby\": false, \"species\": { \"name\": \"charmander\", \"url\": \"https://pokeapi.co/api/v2/pokemon-species/4/\" } }, \"id\": 2 }";

  @Test
  void should_returnAnUrl_when_serviceEvolutionChainWasExecutedSuccessfully() {
    PokemonSpecies pokemonSpecies = new PokemonSpecies();
    EvolutionChain evolutionChain = new EvolutionChain();
    evolutionChain.setUrl("https://pokeapi.co/api/v2/pokemon-species/4/");
    pokemonSpecies.setEvolutionChain(evolutionChain);
    when(restTemplate.getForObject(anyString(), any())).thenReturn(pokemonSpecies);

    PokemonSpecies response = evolutionService.getUrlEvolutionOfAPokemonByUrl("");

    String expected = "https://pokeapi.co/api/v2/pokemon-species/4/";
    String actual = response.getEvolutionChain().getUrl();
    assertEquals(expected, actual);
  }

  @Test
  void should_AListOfEvolutions_when_serviceEvolutionsWasNotExecutedSuccessfully() {
    when(restTemplate.getForObject(anyString(), any())).thenReturn(responsePokeApiEvolveTo());
    ResponsePokeApiEvolveTo response = evolutionService.getAllEvolutionsChainOfAPokemonByUrl("");
    String expected = "charmander";
    String actual = response.getChain().getSpecies().getName();
    assertEquals(expected, actual);
  }

  private ResponsePokeApiEvolveTo responsePokeApiEvolveTo() {
    final Gson gson = new GsonBuilder().create();
    final ResponsePokeApiEvolveTo response = gson.fromJson(jsonEvolutionChain,
        ResponsePokeApiEvolveTo.class);
    return response;
  }
}