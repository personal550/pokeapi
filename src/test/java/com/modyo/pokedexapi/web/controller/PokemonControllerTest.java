package com.modyo.pokedexapi.web.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import com.modyo.pokedexapi.business.service.PokemonService;
import com.modyo.pokedexapi.data.dto.response.evolution.Effect;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponseApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.Specie;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseApiPokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class PokemonControllerTest {
  @Mock
  private PokemonService pokemonService;

  @InjectMocks
  private PokemonController controller = new PokemonController();

  @BeforeEach
  public void setup(){
    when(pokemonService.getByOffsetAndPage(any())).thenReturn(getResponseApiPokemon());
    when(pokemonService.getEvolutionById(anyInt())).thenReturn(getResponseApiEnvolveTo());
  }

  @Test
  void should_returnAListAllEvolutionOfAPokemon_when_evolutionsExist(){
    ResponseEntity<ResponseApiEvolveTo> response = controller.findPokemonById(1);
    int expected = 2;
    int actual = response.getBody().getEvolves().size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnStatusCode404_when_evolutionsDoNotExist(){
    ResponseApiEvolveTo response = getResponseApiEnvolveTo();
    response.setEvolves(new ArrayList<>());
    try{
      controller.findPokemonById(1);
    }catch(ResponseStatusException e){
      String expected = "404 NOT_FOUND \"not evolutions found\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }

  @Test
  void should_returnStatusCode500_when_serviceWasNotExecutedSuccessfully(){
    when(pokemonService.getEvolutionById(anyInt())).thenThrow(
        new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "error"));
    try{
      controller.findPokemonById(1);
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }


  @Test
  void should_returnAListOfPokemon_when_pageExist(){
    ResponseEntity<ResponseApiPokemon> response = controller.findByOffsetAndLimit(1,3);
    int expected = 2;
    int actual = response.getBody().getPokemon().size();
    assertEquals(expected, actual);
  }

  @Test
  void should_returnNotFound_when_pageDoesNotExist(){
    ResponseApiPokemon responseApiPokemon = getResponseApiPokemon();
    responseApiPokemon.setPokemon(new ArrayList<>());
    when(pokemonService.getByOffsetAndPage(any())).thenReturn(responseApiPokemon);
    try{
      controller.findByOffsetAndLimit(1,3);
    }catch(ResponseStatusException e){
      String expected = "404 NOT_FOUND \"not pokemon found\"";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }

  @Test
  void should_throwAnException_when_serviceWasNotExecutedSuccessfully(){
    when(pokemonService.getByOffsetAndPage(any())).thenThrow(
        new ResponseStatusException(HttpStatus.NOT_FOUND, "not envolve to found"));
    try{
      controller.findByOffsetAndLimit(600,3);
    }catch(ResponseStatusException e){
      String expected = "500 INTERNAL_SERVER_ERROR";
      String actual = e.getMessage();
      assertEquals(expected, actual);
    }
  }

  private ResponseApiPokemon getResponseApiPokemon(){
    return ResponseApiPokemon
        .builder()
        .limit(1)
        .nextPage(2)
        .pokemon(new ArrayList<>(){
          {
            add(ResponsePokemon.builder().build());
            add(ResponsePokemon.builder().build());
          }
        })
        .build();
  }

  private ResponseApiEvolveTo getResponseApiEnvolveTo(){
    Specie specie = new Specie();
    specie.setName("rhyhorn");
    specie.setPhoto("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/111.png");
    Specie specie2 = new Specie();
    specie2.setPhoto("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/112.png");
    specie2.setName("rhydon");
    return ResponseApiEvolveTo
        .builder()
        .idPokemon(1)
        .effect(Effect.builder().short_effect("Ensures success fleeing from wild battles.").build())
        .evolves(new ArrayList<>(){
          {
            add(specie);
            add(specie2);
          }
        })
        .build();
  }

}