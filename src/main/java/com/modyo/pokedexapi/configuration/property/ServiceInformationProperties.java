package com.modyo.pokedexapi.configuration.property;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Configuration
@Data
public class ServiceInformationProperties {

  @Value("${host.service}")
  private String urlService;

  @Value("${host.endpointPokemon}")
  private String endpointPokemon;

  @Value("${host.endpointCharacteristic}")
  private String endpointCharacteristic;

  @Value("${host.endpointEvolutions}")
  private String endpointEvolutions;

  @Value("${host.endpointAbility}")
  private String endpointAbility;

  @Value("${host.endpointFindById}")
  private String endpointFindById;

  @Value("${host.limit}")
  private int limit;
}
