package com.modyo.pokedexapi.configuration.resttemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Configuration
public class RestTemplateConfig {
  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }
}
