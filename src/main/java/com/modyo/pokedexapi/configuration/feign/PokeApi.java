package com.modyo.pokedexapi.configuration.feign;


import com.modyo.pokedexapi.data.dto.response.evolution.AbilityEvolution;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokeApi;
import java.util.Optional;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@FeignClient(name = "pokeApi", url = "${host.service}")
public interface PokeApi {
  @RequestMapping(method = RequestMethod.GET, value = "${host.endpointPokemon}")
  Optional<ResponsePokeApi> getByOffsetAndPage(@RequestParam(value = "offset") int offset,
      @RequestParam(value = "limit") int page);

  @RequestMapping(method = RequestMethod.GET, value = "${host.endpointAbility}")
  Optional<AbilityEvolution> getAbility(
      @RequestParam(value = "idPokemon") int idPokemon);
}