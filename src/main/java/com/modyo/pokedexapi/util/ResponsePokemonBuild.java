package com.modyo.pokedexapi.util;

import com.modyo.pokedexapi.data.dto.response.evolution.AbilityEvolution;
import com.modyo.pokedexapi.data.dto.response.evolution.Effect;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseCharacteristicApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

public class ResponsePokemonBuild {

  /**
   * This method create an object ResponsePokemon.
   *
   * @param response
   * @return an object ResponsePokemon
   */
  public static ResponsePokemon createResponsePokemon(ResponseCharacteristicApi response) {
    return ResponsePokemon.builder().name(response.getName())
        .abilities(
            Optional.ofNullable(response.getAbilities()).orElse(new ArrayList<>())
                .stream()
                .map(a -> a.getAbility())
                .collect(Collectors.toList()))
        .photo(getPhotoFromResponse(response))
        .weight(response.getWeight())
        .types(Optional.ofNullable(response.getTypes()).orElse(new ArrayList<>()).stream()
            .map(s -> s.getType()).collect(
                Collectors.toList()))
        .idPokemon(response.getId())
        .species(response.getSpecies())
        .build();
  }

  /**
   * This method that obtain a photo of a pokemon.
   *
   * @param response
   * @return String
   */
  public static String getPhotoFromResponse(ResponseCharacteristicApi response) {
    try{
      Optional<String> photo = Optional.ofNullable(response.getSprites())
          .filter(x -> Optional.ofNullable(response.getSprites().getOther()).isPresent())
          .filter(x -> Optional.ofNullable(response.getSprites().getOther().getHome()).isPresent())
          .filter(x -> Optional.ofNullable(response.getSprites().getOther().getHome().getFrontDefault()).isPresent())
          .map(x -> x.getOther().getHome().getFrontDefault());
      if(photo.isPresent()){
        return photo.orElse("");
      }
      Optional<String> photoArtWord = Optional.ofNullable(response.getSprites())
          .filter(x -> Optional.ofNullable(response.getSprites().getOther()).isPresent())
          .filter(x -> Optional.ofNullable(response.getSprites().getOther().getOfficialArtwork()).isPresent())
          .filter(x -> Optional.ofNullable(response.getSprites().getOther().getOfficialArtwork().getFrontDefault()).isPresent())
          .map(x -> x.getOther().getOfficialArtwork().getFrontDefault());
      return photoArtWord.orElse("");
    }catch(Exception e){
      return "";
    }
  }

  /**
   * This method that obtain a effect of a Pokemon.
   *
   * @param ability
   * @return Effect
   */
  public static Effect getEffectPokemon(Optional<AbilityEvolution> ability) {
    try{
      Effect effect = Effect.builder().build();
      return ability
          .filter(x -> Optional.ofNullable(x.getEffect_entries()).isPresent())
          .filter(x -> !x.getEffect_entries().isEmpty())
          .map(x -> x.getEffect_entries()).orElse(new ArrayList<>())
          .stream()
          .filter(x -> Optional.ofNullable(x.getLanguage()).isPresent())
          .filter(x -> Optional.ofNullable(x.getLanguage().getName()).isPresent())
          .filter(x -> Optional.ofNullable(x.getLanguage().getName()).orElse("").equals("en"))
          .map(x -> Effect.builder().short_effect(x.getShort_effect()).build()).collect(Collectors.toList())
          .get(0);
    }catch(Exception e){
      return null;
    }
  }
}
