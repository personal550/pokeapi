package com.modyo.pokedexapi.util;

import com.modyo.pokedexapi.data.dto.response.evolution.Chain;
import com.modyo.pokedexapi.data.dto.response.evolution.EvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.Specie;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public class InvolveMapper {


  /**
   * This method transform a response of api to a list of species
   *
   * @param response
   * @return a list of species of pokemon
   */
  public static List<Specie> mapInvolveResponseToListEvolves(ResponsePokeApiEvolveTo response){
    List<Specie> evolves = new ArrayList<>();
    Chain chain = Optional.ofNullable(response.getChain()).orElse(new Chain());
    evolves.add(chain.getSpecies());
    evolves.addAll(getEvolutionsFromEvolutionChainEndPoint(chain.getEvolvesTo()));
    return evolves;
  }

  /**
   * This method that iterate inside de evolutions.
   *
   * @param evolveTo
   * @return a list of species of pokemon
   */
  public static List<Specie> getEvolutionsFromEvolutionChainEndPoint(List<EvolveTo> evolveTo){
    if(evolveTo.isEmpty()){
      return new ArrayList<>();
    }
    try{
      return new ArrayList<>(){
        {
          {
            add(new Specie(evolveTo.get(0).getSpecies().getName(),
                    "",
                    ""));
            addAll(getEvolutionsFromEvolutionChainEndPoint(evolveTo.get(0).getEvolvesTo()));
          }
        }
      };
    }catch(Exception e){
      return new ArrayList<>();
    }

  }

}
