package com.modyo.pokedexapi.data.dto.response.pokemon;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@AllArgsConstructor
@Data
public class ResponsePokemon implements Serializable {
  private static final long serialVersionUID = 1L;
  private int idPokemon;
  private String photo;
  private List<Type> types;
  private String weight;
  private String name;
  private List<Ability> abilities;
  @JsonIgnore
  private Species species;


  public ResponsePokemon() {
  }
}
