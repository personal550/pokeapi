package com.modyo.pokedexapi.data.dto.response.evolution;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class Language implements Serializable {
  private String name;
  private String url;
}
