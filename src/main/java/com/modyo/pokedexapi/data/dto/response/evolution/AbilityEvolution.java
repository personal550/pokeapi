package com.modyo.pokedexapi.data.dto.response.evolution;

import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class AbilityEvolution implements Serializable {
  private static final long serialVersionUID = 1L;
  private List<Effect> effect_entries;

  public AbilityEvolution() {
  }

  public AbilityEvolution(
      List<Effect> effect_entries) {
    this.effect_entries = effect_entries;
  }
}
