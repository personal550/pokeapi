package com.modyo.pokedexapi.data.dto.response.evolution;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class ResponsePokeApiEvolveTo implements Serializable {
  private static final long serialVersionUID = 1L;
  private Chain chain;

  public ResponsePokeApiEvolveTo() {
  }

  public ResponsePokeApiEvolveTo(Chain chain) {
    this.chain = chain;
  }
}
