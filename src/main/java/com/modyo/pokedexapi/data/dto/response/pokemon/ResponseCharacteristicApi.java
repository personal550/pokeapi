package com.modyo.pokedexapi.data.dto.response.pokemon;

import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class ResponseCharacteristicApi implements Serializable {
  private static final long serialVersionUID = 1L;
  private int id;
  private List<AbilityCharacteristic> abilities;
  private String weight;
  private List<TypeCharacteristic> types;
  private String name;
  private Sprite sprites;
  private Species species;
}
