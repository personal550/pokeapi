package com.modyo.pokedexapi.data.dto.response.evolution;

import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class EvolutionChain {
  private String url;

  public EvolutionChain() {
  }
}
