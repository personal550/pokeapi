package com.modyo.pokedexapi.data.dto.response.evolution;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class PokemonSpecies {
  @JsonProperty("evolution_chain")
  private EvolutionChain evolutionChain;

  public PokemonSpecies() {
  }
}
