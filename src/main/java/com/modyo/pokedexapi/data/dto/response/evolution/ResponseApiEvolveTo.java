package com.modyo.pokedexapi.data.dto.response.evolution;

import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokeApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class ResponseApiEvolveTo implements Serializable {
  private static final long serialVersionUID = 1L;
  private int idPokemon;
  private List<Specie> evolves;
  private Effect effect;
  private ResponsePokemon pokemon;

  public ResponseApiEvolveTo() {
  }

  public ResponseApiEvolveTo(int idPokemon,
      List<Specie> evolves, Effect effect, ResponsePokemon responsePokemon) {
    this.idPokemon = idPokemon;
    this.evolves = evolves;
    this.effect = effect;
    this.pokemon = responsePokemon;
  }
}
