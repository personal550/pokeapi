package com.modyo.pokedexapi.data.dto.response.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Sprite implements Serializable {
  private static final long serialVersionUID = 1L;
  private Other other;
  @JsonProperty("back_default")
  private String backDefault;
}
