package com.modyo.pokedexapi.data.dto.response.pokemon;

import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Species {
  private String name;
  private String url;

  public Species() {
  }

  public Species(String name, String url) {
    this.name = name;
    this.url = url;
  }
}
