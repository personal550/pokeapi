package com.modyo.pokedexapi.data.dto.response.pokemon;

import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class ResponseApiPokemon implements Serializable {

  private static final long serialVersionUID = 1L;
  private int nextPage;
  private int limit;
  private int totalPages;
  private int count;
  private List<ResponsePokemon> pokemon;

}
