package com.modyo.pokedexapi.data.dto.response.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Other implements Serializable {
  private Home home;

  @JsonProperty("official-artwork")
  private OfficialArtWork officialArtwork;

  public Other() {
  }
}
