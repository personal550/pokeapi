package com.modyo.pokedexapi.data.dto.response.pokemon;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class ResponsePokeApi implements Serializable {
  private static final long serialVersionUID = 1L;
  private int count;
  private String next;
  private String previous;
  private List<Pokemon> results = new ArrayList<>();
}
