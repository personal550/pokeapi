package com.modyo.pokedexapi.data.dto.response.evolution;

import java.io.Serializable;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Specie implements Serializable {
  private static final long serialVersionUID = 1L;
  private String name;
  private String photo;
  private String description;

  public Specie() {
  }

  public Specie(String name, String photo, String description) {
    this.name = name;
    this.photo = photo;
    this.description = description;
  }
}
