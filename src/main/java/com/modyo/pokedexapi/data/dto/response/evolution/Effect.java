package com.modyo.pokedexapi.data.dto.response.evolution;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
@Builder
public class Effect implements Serializable {
  private String short_effect;
  private Language language;
}
