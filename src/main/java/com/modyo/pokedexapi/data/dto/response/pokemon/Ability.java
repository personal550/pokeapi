package com.modyo.pokedexapi.data.dto.response.pokemon;

import java.io.Serializable;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Ability implements Serializable {
  private static final long serialVersionUID = 1L;
  private String name;
  private String url;

  public Ability() {
  }
}
