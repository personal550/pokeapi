package com.modyo.pokedexapi.data.dto.response.evolution;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Chain implements Serializable {
  private static final long serialVersionUID = 1L;
  @JsonProperty("evolves_to")
  private List<EvolveTo> evolvesTo;
  private Specie species;
  private Chain chain;

  public Chain() {
  }
}
