package com.modyo.pokedexapi.data.dto.response.pokemon;

import java.io.Serializable;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class ResponseType implements Serializable {
  private static final long serialVersionUID = 1L;
  private Type type;
}
