package com.modyo.pokedexapi.data.dto.request;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Builder
@AllArgsConstructor
@Data
public class RequestAllPokemon implements Serializable {
  @NotNull(message = "page is is required")
  @Pattern(message = "page must be a numeric", regexp = "^\\d+$")
  private int page;

  @Pattern(message = "limit must be a numeric", regexp = "^\\d+$")
  private int limit;

  public RequestAllPokemon() {
  }
}
