package com.modyo.pokedexapi.data.dto.response.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class Home implements Serializable {
  @JsonProperty("front_default")
  private String frontDefault;

  public Home() {
  }
}
