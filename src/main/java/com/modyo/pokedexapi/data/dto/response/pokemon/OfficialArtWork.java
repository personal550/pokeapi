package com.modyo.pokedexapi.data.dto.response.pokemon;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Data
public class OfficialArtWork {
  @JsonProperty("front_default")
  private String frontDefault;
}
