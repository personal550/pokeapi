package com.modyo.pokedexapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@SpringBootApplication
@EnableFeignClients
public class PokedexapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokedexapiApplication.class, args);
	}

}
