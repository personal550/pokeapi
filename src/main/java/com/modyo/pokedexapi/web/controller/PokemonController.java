package com.modyo.pokedexapi.web.controller;

import com.modyo.pokedexapi.business.service.PokemonService;
import com.modyo.pokedexapi.data.dto.request.RequestAllPokemon;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponseApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseApiPokemon;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@RestController
@RequestMapping("${api.prefix}${api.version}pokemon")
@Api(value = "Pokemon Rest Controller")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class PokemonController {

  @Autowired
  private PokemonService pokemonService;

  /**
   * This method handles the search by page
   *
   * @param page
   * @param limit
   * @return return a object ResponseApiPokemon
   */
  @ApiOperation(value = "Get all pokemon by page")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "list of all pokemon found"),
      @ApiResponse(code = 500, message = "server error"),
      @ApiResponse(code = 404, message = "list empty") })
  @GetMapping("/page/{page}/limit/{limit}")
  public ResponseEntity<ResponseApiPokemon> findByOffsetAndLimit(
      @PathVariable(value = "page") int page, @PathVariable(value = "limit") int limit) {
    RequestAllPokemon requestAllPokemon = RequestAllPokemon.builder().limit(limit).page(page)
        .build();
    ResponseApiPokemon response = null;
    try{
      response = pokemonService.getByOffsetAndPage(requestAllPokemon);
    }catch(Exception e){
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  /**
   * This method handles the search that all species of a specific pokemon
   *
   * @param idPokemon
   * @return return a object ResponseApiEnvolveTo
   */
  @ApiOperation(value = "Get all evolution of a pokemon")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "list of all pokemon found"),
      @ApiResponse(code = 500, message = "server error"),
      @ApiResponse(code = 404, message = "list empty") })
  @GetMapping("/envolve/{idPokemon}")
  public ResponseEntity<ResponseApiEvolveTo> findPokemonById(
      @PathVariable(value = "idPokemon") int idPokemon) {
    ResponseApiEvolveTo response = null;
    try{
      response = pokemonService.getEvolutionById(idPokemon);
    }catch(Exception e){
      throw new ResponseStatusException(
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
