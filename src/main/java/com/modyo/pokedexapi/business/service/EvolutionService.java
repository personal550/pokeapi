package com.modyo.pokedexapi.business.service;

import com.modyo.pokedexapi.data.dto.response.evolution.PokemonSpecies;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface EvolutionService {

  PokemonSpecies getUrlEvolutionOfAPokemonByUrl(String url);

  ResponsePokeApiEvolveTo getAllEvolutionsChainOfAPokemonByUrl(String url);

}
