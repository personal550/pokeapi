package com.modyo.pokedexapi.business.service;

import com.modyo.pokedexapi.data.dto.request.RequestAllPokemon;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponseApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseApiPokemon;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface PokemonService {
  ResponseApiPokemon getByOffsetAndPage(RequestAllPokemon requestAllPokemon);
  ResponseApiEvolveTo getEvolutionById(int idPokemon);
}
