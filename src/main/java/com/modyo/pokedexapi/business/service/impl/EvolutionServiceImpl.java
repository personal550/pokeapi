package com.modyo.pokedexapi.business.service.impl;

import com.modyo.pokedexapi.business.service.EvolutionService;
import com.modyo.pokedexapi.data.dto.response.evolution.PokemonSpecies;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Service
public class EvolutionServiceImpl implements EvolutionService {

  @Autowired
  private RestTemplate restTemplate;

  /**
   * This method search the url for evolution of a Pokemon by url.
   *
   * @param url
   * @return return an object PokemonSpecies
   */
  @Override
  public PokemonSpecies getUrlEvolutionOfAPokemonByUrl(String url) {
    return Optional.ofNullable(
            restTemplate.getForObject(url, PokemonSpecies.class))
        .orElse(new PokemonSpecies());
  }

  /**
   * This method get all evolutions of a Pokemon by url.
   *
   * @param url
   * @return return an object PokemonSpecies
   */
  @Override
  public ResponsePokeApiEvolveTo getAllEvolutionsChainOfAPokemonByUrl(String url) {
    return Optional.ofNullable(
            restTemplate.getForObject(url, ResponsePokeApiEvolveTo.class))
        .orElse(new ResponsePokeApiEvolveTo());
  }
}
