package com.modyo.pokedexapi.business.service;

import com.modyo.pokedexapi.data.dto.response.pokemon.Pokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseCharacteristicApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import java.util.List;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
public interface CharacteristicService {

  List<ResponsePokemon> getCharacteristicsFromListOfPokemon(List<Pokemon> pokemon);

  ResponsePokemon getCharacteristicsByIdPokemon(int idPokemon);

  ResponseCharacteristicApi getAllInformationOfAPokemon(String namePokemon);
}
