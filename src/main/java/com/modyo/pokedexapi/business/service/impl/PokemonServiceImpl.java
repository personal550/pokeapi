package com.modyo.pokedexapi.business.service.impl;

import com.modyo.pokedexapi.business.service.CharacteristicService;
import com.modyo.pokedexapi.business.service.EvolutionService;
import com.modyo.pokedexapi.business.service.PokemonService;
import com.modyo.pokedexapi.configuration.feign.PokeApi;
import com.modyo.pokedexapi.configuration.property.ServiceInformationProperties;
import com.modyo.pokedexapi.data.dto.request.RequestAllPokemon;
import com.modyo.pokedexapi.data.dto.response.evolution.AbilityEvolution;
import com.modyo.pokedexapi.data.dto.response.evolution.Effect;
import com.modyo.pokedexapi.data.dto.response.evolution.Language;
import com.modyo.pokedexapi.data.dto.response.evolution.PokemonSpecies;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponseApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.ResponsePokeApiEvolveTo;
import com.modyo.pokedexapi.data.dto.response.evolution.Specie;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseApiPokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseCharacteristicApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokeApi;
import com.modyo.pokedexapi.util.InvolveMapper;
import com.modyo.pokedexapi.util.ResponsePokemonBuild;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Service
@Slf4j
public class PokemonServiceImpl implements PokemonService {

  @Autowired
  private PokeApi pokeApi;

  @Autowired
  private ServiceInformationProperties serviceInformation;

  @Autowired
  private EvolutionService evolutionService;

  @Autowired
  private CharacteristicService characteristicService;

  /**
   * This method that search all pokemon available in poke api server
   *
   * @param requestAllPokemon
   * @return return a object ResponseApiPokemon
   */
  @Override
  public ResponseApiPokemon getByOffsetAndPage(RequestAllPokemon requestAllPokemon) {
    int nextPage = requestAllPokemon.getPage() + 1;
    requestAllPokemon.setPage((requestAllPokemon.getPage() - 1) * requestAllPokemon.getLimit());
    ResponsePokeApi response = pokeApi.getByOffsetAndPage(requestAllPokemon.getPage(),
        requestAllPokemon.getLimit()).orElse(ResponsePokeApi.builder().build());
    double totalPages = response.getCount() / requestAllPokemon.getLimit();
    List<ResponsePokemon> listPokemon = characteristicService.getCharacteristicsFromListOfPokemon(
        response.getResults());
    return ResponseApiPokemon
        .builder()
        .count(response.getCount())
        .totalPages((int) Math.ceil(totalPages))
        .nextPage(nextPage)
        .pokemon(listPokemon)
        .limit(requestAllPokemon.getLimit())
        .build();
  }

  /**
   * This method that search all evolutions of a pokemon.
   *
   * @param idPokemon
   * @return return an object ResponseApiEvolveTo
   */
  @Override
  public ResponseApiEvolveTo getEvolutionById(int idPokemon) {
    ResponsePokemon listPokemon = characteristicService.getCharacteristicsByIdPokemon(idPokemon);
    Optional<PokemonSpecies> getUrlEvolutionChain = Optional.ofNullable(
        evolutionService.getUrlEvolutionOfAPokemonByUrl(
            listPokemon.getSpecies().getUrl()));
    Optional<ResponsePokeApiEvolveTo> getEvolutionChain = Optional.empty();
    if (getUrlEvolutionChain.isPresent()) {
      Optional<String> url = Optional.ofNullable(getUrlEvolutionChain.get().getEvolutionChain())
          .filter(x -> x.getUrl() != null)
          .map(x -> x.getUrl());
      if (url.isPresent()) {
        getEvolutionChain = Optional.ofNullable(
            evolutionService.getAllEvolutionsChainOfAPokemonByUrl(url.get()));
      }
    }
    List<Specie> evolves = new ArrayList<>();
    if (getEvolutionChain.isPresent()) {
      evolves = InvolveMapper.mapInvolveResponseToListEvolves(getEvolutionChain.get());
    }

    List<Specie> evolvesFinal = evolves.stream()
        .map(p -> Optional.ofNullable(characteristicService.getAllInformationOfAPokemon(p.getName()))
            .orElse(new ResponseCharacteristicApi()))
        .map(p -> new Specie(p.getName(), ResponsePokemonBuild.getPhotoFromResponse(p),""))
        .collect(Collectors.toList());

    Optional<AbilityEvolution> ability = null;
    try{
      ability = pokeApi.getAbility(idPokemon);
    }catch(Exception e){
      ability = Optional.ofNullable(
          AbilityEvolution.builder().effect_entries(new ArrayList<>(){
            {
              add(Effect.builder().short_effect("")
                  .language(Language.builder().url("").name("").build()).build());
            }
          }).build());
      log.info("poke api exception found because : ", e.getMessage());
    }

    return ResponseApiEvolveTo
        .builder()
        .idPokemon(idPokemon)
        .evolves(evolvesFinal)
        .effect(ResponsePokemonBuild.getEffectPokemon(ability))
        .pokemon(listPokemon)
        .build();
  }
}
