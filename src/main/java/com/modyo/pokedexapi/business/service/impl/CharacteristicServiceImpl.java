package com.modyo.pokedexapi.business.service.impl;

import com.modyo.pokedexapi.business.service.CharacteristicService;
import com.modyo.pokedexapi.configuration.property.ServiceInformationProperties;
import com.modyo.pokedexapi.data.dto.response.pokemon.Pokemon;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponseCharacteristicApi;
import com.modyo.pokedexapi.data.dto.response.pokemon.ResponsePokemon;
import com.modyo.pokedexapi.util.ResponsePokemonBuild;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author galindoaguilarf
 * @version 1.0
 */
@Service
public class CharacteristicServiceImpl implements CharacteristicService {

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private ServiceInformationProperties serviceInformationProperties;

  /**
   * This method search than search all characteristics from a list of Pokemon
   *
   * @param pokemon
   * @return return an object List<ResponsePokemon>
   */
  public List<ResponsePokemon> getCharacteristicsFromListOfPokemon(List<Pokemon> pokemon) {
    return Optional.ofNullable(pokemon).orElse(new ArrayList<>())
        .stream()
        .map(p -> Optional.ofNullable(
                restTemplate.getForObject(p.getUrl(), ResponseCharacteristicApi.class))
            .orElse(new ResponseCharacteristicApi()))
        .map(p -> ResponsePokemonBuild.createResponsePokemon(p))
        .collect(Collectors.toList());
  }

  /**
   * This method search all characteristic of a Pokemon
   * and out is response api.
   *
   * @param idPokemon
   * @return return an object ResponsePokemon
   */
  @Override
  public ResponsePokemon getCharacteristicsByIdPokemon(int idPokemon) {
    String url = String.format("%s%s%s", serviceInformationProperties.getUrlService(),
        serviceInformationProperties.getEndpointFindById(), idPokemon);
    ResponseCharacteristicApi response = Optional.ofNullable(
            restTemplate.getForObject(url, ResponseCharacteristicApi.class))
        .orElse(new ResponseCharacteristicApi());
    return ResponsePokemonBuild.createResponsePokemon(response);
  }

  /**
   * This method search all characteristic of a Pokemon from poke api.
   *
   * @param namePokemon
   * @return return an object ResponseCharacteristicApi
   */
  @Override
  public ResponseCharacteristicApi getAllInformationOfAPokemon(String namePokemon) {
    String url = String.format("%s%s%s", serviceInformationProperties.getUrlService(),
        serviceInformationProperties.getEndpointFindById(), namePokemon);
    return Optional.ofNullable(
            restTemplate.getForObject(url, ResponseCharacteristicApi.class))
        .orElse(new ResponseCharacteristicApi());
  }

}
